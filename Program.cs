﻿using System;

namespace ex_15
{
    class Program
    {
        static void Main(string[] args)
        {
            // --------------------------------------------
            // Exercise #15 - Multiplication Tables
            // --------------------------------------------
            // It is possible to reuse a variable - that is kind of the point in many cases.
            // Create a new project (including repository and the dotnet commands and np snippet)
            // Ask the user to type in a number and display the results of a multiplication table from 1 to 12
            // Display one calculation and result on each line.

            bool isFished = false;       // init this value to false

            // Set a variable needed.
            var num = 0;
            //var secondNum = 0;
            Boolean isRepeated = false;

            // Get user's birthday
            Console.WriteLine("-----------------------------------");
            Console.WriteLine(" Let's play multiplication table");
            Console.WriteLine("-----------------------------------");
            
            while(!isFished)
            {
                Console.Write("Please type in a number for multiplication table (1~12) : ");
                num = int.Parse(Console.ReadLine());
                Console.WriteLine();

                for(var i=1;i<=12;i++)
                {
                    Console.WriteLine($"{num} x {i} = {num*i}");
                }

                // --------------------------------------------
                // Exercise #17 - Creating a simple app with a single variable
                // --------------------------------------------
                Console.WriteLine("Do you want to repeat this game(Yes/No)?");
                isRepeated = Console.ReadLine().ToLower() == "yes" ? true : false;

                // Finish this game
                if(!isRepeated)
                {
                    Console.WriteLine("Thank you!\n\n");
                    isFished = true;
                }
                // user want to repeat to play
                else{ }
            }
        }
    }
}
